global.jQuery = require("jquery");
const $ = jQuery;
const _ = require('lodash');
require('modaal');

// バージョン確認
;;; console.log('jQuery: ', $().jquery);
;;; console.log('Lodash: ', _.VERSION);
;;; console.log('modalSrc: ', modalSrc);
;;; console.log('modalOpt: ', modalOpt);

// Add modal CSS
let head = document.getElementsByTagName('head')[0];
let link = document.createElement('link');
const src = "https://cdn.jsdelivr.net/npm/modaal@0.4.4/source/css/modaal.min.css"
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = src;
// Aboid double load
let haveCss = false;
for (const l of head.getElementsByTagName('link')) {
  if (l.href == link.href) {
    haveCss = true
  }
}
if  (!haveCss) {
  head.appendChild(link);
}
// document.body.appendChild(link);

//  <a href="#inline" class="inline">Open</a>
const elmA = document.createElement('a');
if (typeof modalSrc !== 'undefined') {
  elmA.href = modalSrc;
} else {
  elmA.href = "https://timer.onl.jp/"
}
elmA.classList.add("inline");
elmA.id = "clickme";
// elmA.text="Open";
// elmDiv.setAttribute("style", "display:none;")
document.querySelector('body').append(elmA);


// // <div id="inline">    Inline content goes here...</div>
// const elmDiv = document.createElement('div');
// elmDiv.id = "inline";
// elmDiv.setAttribute("style", "display:none;")
// elmDiv.innerText = "Inline content goes here";
// document.querySelector('body').append(elmDiv);
if (typeof modalOpt !== 'undefined') {
  $(".inline").modaal(modalOpt);
} else {
  $(".inline").modaal({
    type: 'iframe',
    width: 700,
    height: 500
  });
}
//  $(".inline").modaal({
//    content_source: '#inline'
//  });

document.getElementById("clickme").click();
