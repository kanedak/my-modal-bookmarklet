#!/usr/bin/env node

const fs = require('fs');
// const readline = require('readline');
const UglifyJS = require("uglify-es");


const regex = /^/;
fs.readFile(process.argv[2], 'utf-8', (err, code) => {
  if (err) throw err;
  console.log(UglifyJS.minify(code).code.replace(regex, 'javascript:'))
});

// const rl = readline.createInterface({
//   input: process.stdin,
//   output: null,
//   terminal: false
// });


