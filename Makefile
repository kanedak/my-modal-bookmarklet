
all: build start

build:
	browserify main.js -o bundle.js

start:
	python3 -m http.server 8000


clean:
	$(RM) bundle.js
	$(RM) -r public
